<?php

use Illuminate\Database\Seeder;

class CustomerEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * This database seeds file would populate the pivot table for Customers & Events tables,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
