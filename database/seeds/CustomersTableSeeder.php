<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * This database seeds file would populate the Customers table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
