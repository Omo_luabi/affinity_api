<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     * This database seeds file would populate the Users table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => 'raymoneto',
            'email' => 'raymoneto@yahoo.com',
            'password' => bcrypt('secret'),
        ]);
        /**

        // Perform some insert into the table
        DB::table('users')->insert([
        	'username' => 'affinity_customer',
            'password' => bcrypt('secret'),
           'group_id' => 3,
            'details_id' => '',
        ]);

        DB::table('users')->insert([
        	'username' => 'affinity_merchant',
            'password' => bcrypt('secret'),
           'group_id' => 2,
            'details_id' => '',
        ]);

        DB::table('users')->insert([
        	'username' => 'affinity_admin',
            'password' => bcrypt('secret'),
           'group_id' => 1,
            'details_id' => '',
        ]);
        **/
    }
}
