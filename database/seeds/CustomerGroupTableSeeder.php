<?php

use Illuminate\Database\Seeder;

class CustomerGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * This database seeds file would populate the pivot table for Customers & Groups tables,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
