<?php

use Illuminate\Database\Seeder;

class UserGroupsTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     * This database seeds file would populate the UserGroups table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
        	'group' => 'admin',
        ]);

        DB::table('user_groups')->insert([
        	'group' => 'merchant',
        ]);

        DB::table('user_groups')->insert([
        	'group' => 'customer',
        ]);
    }
}
