<?php

use Illuminate\Database\Seeder;

class MerchantCategoriesTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     * This database seeds file would populate the MerchantCategoories table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {

        DB::table('merchant_categories')->insert([
        	'name' => 'Gold',
        ]);

        DB::table('merchant_categories')->insert([
        	'name' => 'Silver',
        ]);

        DB::table('merchant_categories')->insert([
        	'name' => 'Bronze',
        ]);

    }
}
