<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     * This database seeds file would populate the Transactions table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
