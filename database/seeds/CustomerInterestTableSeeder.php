<?php

use Illuminate\Database\Seeder;

class CustomerInterestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * This database seeds file would populate the pivot table for Customers & Interests tables,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
