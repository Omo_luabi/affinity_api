<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * This database seeds file would populate the Groups table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
        	'name' => 'Smart Ladies',
            'creator_id' => '',
            'details' => 'Smart Ladies',
            'group_head_id' => '',
        ]);

        DB::table('groups')->insert([
        	'name' => 'Vision',
            'creator_id' => '',
            'details' => 'Vision Group',
            'group_head_id' => '',
        ]);

    }
}
