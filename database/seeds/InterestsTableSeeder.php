<?php

use Illuminate\Database\Seeder;

class InterestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * This database seeds file would populate the Interests table,
     * with some dummy data.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests')->insert([
        	'name' => 'Information Technology',
        ]);

        DB::table('interests')->insert([
        	'name' => 'Finance',
        ]);

        DB::table('interests')->insert([
        	'name' => 'Building & Construction',
        ]);
    }
}
