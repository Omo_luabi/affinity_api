<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerInterestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_interest', function (Blueprint $table) {
            $table->char('customer_id',20);
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');

            $table->integer('interest_id')->unsigned();
            $table->foreign('interest_id')->references('interest_id')->on('interests')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_interest');
    }
}
