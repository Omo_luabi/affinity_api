<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->char('merchant_id',20)->unique();
            $table->integer('category_id');
            $table->foreign('category_id')->references('category_id')->on('merchant_categories')->onDelete('cascade');


            $table->string('name');
            $table->text('address');
            $table->char('contact',20)->unique();
            $table->string('email');
            $table->string('longlat');
            $table->char('verification_pin',10);
            $table->text('avatar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
