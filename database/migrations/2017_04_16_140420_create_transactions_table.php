<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->char('customer_id',20);
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');

            $table->char('merchant_id',20);
            $table->foreign('merchant_id')->references('merchant_id')->on('merchants')->onDelete('cascade');

            $table->decimal('amount',6,2);
            $table->string('remarks')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
