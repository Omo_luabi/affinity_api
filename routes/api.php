<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1/merchants'], function(){
	
	Route::resource('categories','MerchantCategoryController');

	Route::get('categories/{id}/merchants','MerchantCategoryController@getMerchants');

    Route::get('convert', 'MerchantController@convert');
	Route::get('around/{lnglat}','MerchantController@around');
	Route::resource('offers','MerchantOfferController');
	Route::resource('experiences','ExperiencesController');
	Route::put('offers/{id}','MerchantOfferController@update');
});

Route::group(['prefix' => 'v1'], function(){
	
	Route::post('login','LoginController@authenticate');

	Route::put('users/{id}','UserController@updateByDetailsId')->where('id','[A-Za-z0-9]+');
	
	Route::get('users/check/{id}','UserController@confirmUsername')->where('id','[A-Za-z0-9_]+');

	Route::resource('users','UserController');

	Route::resource('usergroups','UserGroupController');

    Route::get('customers/{id}/briefs','CustomerController@getBriefs');

	Route::get('customers/{id}/interests','CustomerController@getInterests');

	Route::get('customers/{id}/events','CustomerController@getEvents');

	Route::get('customers/events/{id}','CustomerController@getJoinedEvents');

	Route::get('customers/groups/{id}','CustomerController@getJoinedGroups');

	Route::get('customers/{id}/groups','CustomerController@getGroups');
	
	Route::get('customers/{id}/unique_groups','CustomerController@getUniqueGroups');

	Route::get('customers/{id}/reviews','CustomerController@getReviews');

	Route::get('customers/{id}/transactions','CustomerController@getTransactions');

	Route::post('customers/group/join', 'CustomerController@joinGroup');

	Route::post('customers/event/join', 'CustomerController@joinEvent');

	Route::resource('customers','CustomerController');

	Route::get('merchants/{id}/reviews','MerchantController@getReviews');

	Route::get('merchants/{id}/transactions','MerchantController@getTransactions');

	Route::get('merchants/{id}/offers','MerchantController@getOffers');

	Route::post('users/upload','CustomerController@uploadImage');


	Route::resource('merchants','MerchantController');


	Route::resource('interests','InterestController');

	Route::get('interests/{id}/customers','InterestController@getCustomers');


	Route::resource('reviews','ReviewController');

	Route::resource('transactions','TransactionController');


	Route::resource('events','EventController');

	Route::resource('groups','GroupController');

	Route::get('groups/{id}/customers','GroupController@getCustomers');

	Route::get('groups/{id}/events','GroupController@getEvents');

	Route::get('events/{id}/customers','EventController@getCustomers');

	Route::get('admin/summary','CustomerController@getAdminSummary');
});