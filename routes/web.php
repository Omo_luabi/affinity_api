<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/images/{filename}', function ($filename)
{
    $path = storage_path() . '/app/images/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = "";
    switch(File::extension($filename)){
        case "png":
            $type = "image/png";
        case "jpg":
            $type = "image/jpg";
        case "jpeg":
            $type = "image/jpeg";
    }
    //$type = File::mimeType($file);//"image/png";//File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('avatar');



