<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    protected $primaryKey = 'interest_id';

    /**
     * An interest has many customers.
     * Get customers associated with the interest.
     *
     * @return \Illuminate\Database\Eloquent\BelongsToMany
     */
    public function customers()
    {
        return $this->belongsToMany('App\Customer','customer_interest','interest_id','customer_id');
    }
}
