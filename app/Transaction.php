<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'customer_id',
    	'merchant_id',
    	'amount',
    	'remarks'
    ];


    protected $primaryKey = 'transaction_id';

    /**
     * A transaction belongs to a merchant.
     * Get merchant associated with the transaction.
     *
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }

    /**
     * A transaction belongs to a customer.
     * Get merchant associated with the transaction.
     *
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
