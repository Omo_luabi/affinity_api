<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['group'];


    protected $primaryKey = 'group_id';

    /**
     * A group has many users.
     * Get users associated with the group.
     *
     * @return \Illuminate\Database\Eloquent\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

}
