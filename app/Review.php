<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'customer_id',
    	'review',
    	'rating',
    	'merchant_id'

    ];


    protected $primaryKey = 'review_id';

    /**
     * A review belongs to a merchant.
     * Get merchant associated with the review.
     *
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }

    /**
     * A review belongs to a customer.
     * Get customer associated with the review.
     *
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
