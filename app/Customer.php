<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
    	'customer_id',
    	'address',
    	'phone',
    	'email',
    	'status',
    	'sex',
    	'city',
    	'country'
    	];

    protected $primaryKey = 'customer_id';

    public $incrementing = false;

    /**
     * A Customer can have many interests.
     * Get interests associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\BelongsToMany
     */
    public function interests()
    {
        return $this->belongsToMany('App\Interest','customer_interest','customer_id','interest_id')->withTimestamps();
    }


    /**
     * A Customer can join many groups.
     * Get groups associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group','customer_group','customer_id','group_id')->withTimestamps();
    }

    /**
     * A Customer can attend many events.
     * Get events associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany('App\Event','customer_event','customer_id','event_id')->withTimestamps();
    }

    /**
     * A Customer can have many reviews.
     * Get reviews associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Review','customer_id');
    }


    /**
     * A Customer can have many transactions.
     * Get transactions associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('App\Transaction','customer_id');
    }

}
