<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
    	'password',
    	'group_id',
    	'details_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user belongs to a user group.
     * Get group associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\UserGroup');
    }

    /**
     * Find user by details id.
     *
     * @param  int  $id
     */
    public static function findByDetailsId($id)
    {
        //
        $user = User::where('details_id',$id)->first();

        return $user;
    }

    /**
     * Confirm if this username has not been taken
     *
     * @param  String  $username
     */
    public static function confirmUsername($username)
    {
        //
        $user = User::where('username',$username)->first();

        return $user;
    }
}
