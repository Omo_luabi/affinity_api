<?php

namespace App\Http\Controllers;

use App\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $userGroups = UserGroup::all();

        return response()->json(['error' => false, 'usergroups' => $userGroups], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $group = new UserGroup;

        $group->group = $request->input('group');

           if ($group->save())
           {
                return response()->json(['error' => false, 'message' => 'Group added successfully'],200);
           }

        return response()->json(['error' => true, 'message' => 'Error adding new usergroup'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
        // show a particular user
        $group = UserGroup::findOrFail($id);

        return response()->json(['error' => false, 'group' => $group],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {

            $group = UserGroup::findOrFail($id);

            $group->group = $request->input('group');

           if ($group->save())
           {
                return response()->json(['error' => false, 'message' => 'Group updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating usergroup record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $group = UserGroup::findOrFail($id);


            if ($user->delete())
            {

            return response()->json(['error' => false, 'message' => 'UserGroup record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'UserGroup record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }
}
