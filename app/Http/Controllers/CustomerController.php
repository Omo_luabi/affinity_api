<?php

namespace App\Http\Controllers;

use App\MerchantOffer;
use App\Merchant;
use App\Group;
use App\Event;
use App\Customer;
use App\User;



use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customers = Customer::all();


        return response()->json(['error' => false, 'customers' => $customers],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $customer = new Customer;

        // fake customer id...

        $customer->customer_id = $this->generatefakeID();
        //$customer->customer_id = $request->input('customer_id');

        $customer->name = $request->input('name');

        $customer->address = $request->input('address');

        $customer->phone = $request->input('phone');

        $customer->email = $request->input('email');

        $customer->status = 'Active';

        if ($request->has('city'))
            $customer->city = $request->input('city');

        if ($request->has('country'))
            $customer->country = $request->input('country');

        if ($request->has('sex'))
            $customer->sex = $request->input('sex');

        $interests = ($request->input('interests'));
        
        $avatar = $request->file('avatar'); 

        if($request->has('avatar')){
            $customer->avatar = $request->input('avatar');
        }else if ($request->hasFile('avatar')){
            $extension = $avatar->extension();
    
           // $timestamp = Carbon::now();
            $timestamp = time();
    
            $filename = 'CS_' . $customer->creator_id . $timestamp . '.' . $extension; 
    
            $path = $avatar->storeAs('images', $filename);
    
            if ($request->file('avatar')->isValid()) {
                
                $customer->avatar = $path;
    
            }
        }

       // $user->save();
        /*

        $userLoginRequest = $request->only(['username','password']);

        $group_id = 1; 

        $details_id = $request->customer_id;

       //$user->store($request->only(['username','password']));


        $user->save(array_merge($request->only(['username','password']),['group_id' => $group_id, 'details_id' => $details_id]));*/

        // save the two objects...
        if ($customer->save())
        {
            $customer = Customer::find($customer->customer_id);

            $customer->customer_id = $this->generateCustomerID($customer->id);   

            $customer->save(); 

            // Create Login account

            $user = new User;

            if ($request->has('username')){
                $user->username = $request->input('username');
            }else{
                $user->username = $customer->customer_id;    
            }
            
            $user->password = bcrypt($request->input('password'));
            $user->group_id = 1; // 1 for customer profile
            $user->details_id = $customer->customer_id;    

            $user->save();

        // Add customer interests to the pivot table

            foreach ($interests as $interest)
            {
                $customer->interests()->attach($interest);
            }

            return response()->json(['error' => false, 'message' => 'Record added successfully with ID: '. $customer->customer_id, 'details_id'=>$customer->customer_id],200);
        }

        return response()->json(['error' => true, 'message' => 'Cannot insert record'],400);

    }

    /**
    *API to upload only image
    */
    public function uploadImage(Request $request){
        $type = $request->input('type');
        $avatar = $request->file('avatar');
        $img_path = ""; 

        if ($request->hasFile('avatar')){
            $extension = $avatar->extension();
    
           // $timestamp = Carbon::now();
            $timestamp = time();
    
            $filename = $type . $timestamp . '.' . $extension; 
            //$filename = 'no_image.' . $extension; 
    
            $path = $avatar->storeAs('images', $filename);
    
            if ($request->file('avatar')->isValid()) {
                
                $img_path = $path;
                return response()->json(['error' => false, 'path' => $img_path],200);
            }
        }

        return response()->json(['error' => true, 'message' => 'Cannot upload image'],400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            $customer = Customer::findOrFail($id);

            return response()->json(['error' => false, 'customer' => $customer],200);

        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {
            $customer = Customer::findOrFail($id);

            if ($request->has('name'))
                $customer->name = $request->input('name');

            if ($request->has('address'))
                $customer->address = $request->input('address');

            if ($request->has('phone'))
                $customer->phone = $request->input('phone');

            if ($request->has('email'))
                $customer->email = $request->input('email');

            if ($request->has('status'))
                $customer->status = $request->input('status');

            if ($request->has('city'))
                $customer->city = $request->input('city');

            if ($request->has('country'))
                $customer->country = $request->input('country');

            if ($request->has('sex'))
                $customer->sex = $request->input('sex');

            if($request->has('avatar')){
                $customer->avatar = $request->input('avatar');
            }else if ($request->hasFile('avatar')){
                $avatar = $request->file('avatar'); 
                $extension = $avatar->extension();
        
               // $timestamp = Carbon::now();
                $timestamp = time();
        
                $filename = 'CS_' . $customer->creator_id . $timestamp . '.' . $extension; 
        
                $path = $avatar->storeAs('images', $filename);
        
                if ($request->file('avatar')->isValid()) {
                    
                    $customer->avatar = $path;
        
                }
            }
        
           if ($customer->save())
           {
                return response()->json(['error' => false, 'message' => 'Customer updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating customer record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }
    
    /**
     * Function to get short details for home page
     * 
     * @param varchar $customer_id
     * @return \Illuminate\Http\Response
     */
    public function getBriefs($customer_id){
        try
        {

            //$offers = MerchantOffer::orderBy('created_at', 'desc')->take(4)->get();
            $offers = DB::table('merchant_offers')->join('merchants', 'merchants.merchant_id', '=', 'merchant_offers.merchant_id')
        ->select('merchant_offers.*', 'merchants.*')->orderBy('merchant_offers.created_at', 'desc')->take(4)->get();
        
            $merchants = Merchant::orderBy('created_at', 'desc')->take(4)->get();
            $groups = Group::orderBy('created_at', 'desc')->take(4)->get();
            $events = Event::orderBy('created_at', 'desc')->take(4)->get();
            return response()->json(['error' => false, 'response' =>[ 'offers' => $offers, 
            'merchants' => $merchants,
            'groups' => $groups,
            'events' => $events]], 200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    public function getAdminSummary(){
        //members count grouped by created_at
        //transaction
        //transactions, count of customer grouped by merchants join merchants
        // try
        // {
        //     $dailyMembers = DB::table('customers')
        //             ->select('customers.*', DB::raw('count(*) as count'))
        //             ->groupBy('created_at')
        //             ->get()->toArray();

        //     $merchants = Merchant::orderBy('created_at', 'desc')->take(4)->get();
        //     return response()->json(['error' => false, 'response' =>[ 
        //     'customers' => $dailyMembers,
        //     'merchants' => $merchants]], 200);

        // }catch (ModelNotFoundException $ex){
        //     return response()->json(['error' => true, 'message' => 'Record not found'],404);
        // }
    }


    /**
     * Function to fetch customer interests
     *
     * @param  varchar  $customer_id
     * @return \Illuminate\Http\Response
     */
    public function getInterests($customer_id)
    {
        //
        try
        {

        $customer = Customer::findOrFail($customer_id);

        $interests = $customer->interests()->get()->toArray(); 

        return response()->json(['error' => false, 'interests' => $interests],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }       
    } 

    /**
     * Function to fetch customer transactions
     *
     * @param  varchar  $customer_id
     * @return \Illuminate\Http\Response
     */
    public function getTransactions($customer_id)
    {
        //
        try
        {

            $customer = Customer::findOrFail($customer_id);

            $transactions = $customer->transactions()->get()->toArray(); 

            return response()->json(['error' => false, 'transactions' => $transactions],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }       
    } 

    /**
     * Function to fetch customer reviews
     *
     * @param  varchar  $customer_id
     * @return \Illuminate\Http\Response
     */
    public function getReviews($customer_id)
    {
        //
        try
        {

            $customer = Customer::findOrFail($customer_id);

            $reviews = $customer->reviews()->get()->toArray(); 

            return response()->json(['error' => false, 'reviews' => $reviews],200); 
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }      
    } 

    /**
     * Function to fetch customer groups
     *
     * @param  varchar  $customer_id
     * @return \Illuminate\Http\Response
     */
    public function getGroups($customer_id)
    {
        //
        try
        {

        $customer = Customer::findOrFail($customer_id);

        $groups = $customer->groups()->get()->toArray(); 

        return response()->json(['error' => false, 'groups' => $groups],200);       
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    } 
    
    
    // fetch groups that the user deosnt belong to
    public function getUniqueGroups($customer_id)
    {
        try
        {
             $groups = DB::select('SELECT * FROM groups WHERE group_id
             NOT IN (SELECT group_id FROM customer_group WHERE customer_id = ?)',[$customer_id]);
    
            return response()->json(['groups'=> $groups],200);
        }catch (Exception $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
        
    } 
    
    /**
     * Function to handle join group by a customer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $group_id
     * @return \Illuminate\Http\Response
     */
    public function joinGroup(Request $request)
    {
        //
        $customer_id = $request->input('customer_id');

        $group_id = $request->input('group_id');

        try
        {

        $customer = Customer::findOrFail($customer_id);

        $customer->groups()->attach($group_id);

        return response()->json(['error' => false, 'message' => 'Customer ' . $customer_id . 'joined group with id: ' . $group_id],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }       
    }  


    /**
     * Function to fetch customer events
     *
     * @param  varchar  $customer_id
     * @return \Illuminate\Http\Response
     */
    public function getEvents($customer_id)
    {
        //
        try
        {

        $customer = Customer::findOrFail($customer_id);

        $events = $customer->events()->get()->toArray(); 

        return response()->json(['error' => false, 'events' => $events],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }       
    }



    public function getJoinedEvents($id)
    {
/*        $events = DB::table('events')
                    ->leftJoin('customer_event','events.event_id','=','customer_event.event_id')

                    ->select('events.*','customer_event.customer_id')
                    ->where('customer_event.customer_id','=','cs1')
                    ->get()->toArray();*/

        $events = DB::table('events')
                    ->leftJoin('customer_event', function ($join) use ($id){
                        $join->on('events.event_id','=','customer_event.event_id')
                            ->where('customer_event.customer_id','=',$id);
                    })

                    ->select('events.*','customer_event.customer_id')
                    
                    ->get()->toArray();

        return response()->json(['events'=> $events],200);
    }

    public function getJoinedGroups($id)
    {

        $groups = DB::table('groups')
                    ->leftJoin('customer_group', function ($join) use ($id){
                        $join->on('groups.group_id','=','customer_group.group_id')
                            ->where('customer_group.customer_id','=',$id);
                    })

                    ->select('groups.*','customer_group.customer_id')
                    
                    ->get()->toArray();

        return response()->json(['groups'=> $groups],200);
    }

    /**
     * Function to handle join event by a customer
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function joinEvent(Request $request)
    {
        //
        $customer_id = $request->input('customer_id');

        $event_id = $request->input('event_id');

        try
        {

        $customer = Customer::findOrFail($customer_id);

        $customer->events()->attach($event_id);

        //return $customer;
        
        return response()->json(['error' => false, 'message' => 'Customer ' . $customer_id . 'attending event id: ' . $event_id],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $customer = Customer::findOrFail($id);


            if ($customer->delete())
            {


            $user = User::findByDetailsId($id);

            $user->delete();

            return response()->json(['error' => false, 'message' => 'Customer record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Customer record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Function to generate Customer ID.
     *
     * @return Response
     */
    public function generateCustomerID($id)
    {
     
        $customIdentifier = new CustomIdentifier;

        $generatedID = $customIdentifier->generateCustomID($id, "Customer");

        return $generatedID;

    }

    public function generatefakeID()
    {
     
        $fake = time();

        return $fake;
    }
}
