<?php

namespace App\Http\Controllers;

use App\User;
use App\Merchant;
use App\MerchantCategory;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Exception;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $merchants = Merchant::orderBy('created_at', 'desc')->get();//Merchant::all();
        
        $categories = MerchantCategory::orderBy('name', 'asc')->get();

        return response()->json(['error' => false, 'merchants' => $merchants, 'categories' => $categories],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $merchant = new Merchant;
        $user = new User;
        
        if($user->confirmUsername($request->input('username')) != null){
            return response()->json(['error' => true, 'message' => "Username has been taken, please choose a different one."],400);
        }
        
        //$merchant->merchant_id = generateMerchantID();

        $merchant->merchant_id = $this->generatefakeID();

        $merchant->category_id = $request->input('category_id');

        $merchant->name = $request->input('name');

        $merchant->address = $request->input('address');

        $merchant->contact = $request->input('contact');

        $merchant->email = $request->input('email');

        $merchant->longlat = $request->input('longlat');

        $merchant->verification_pin = $request->input('verification_pin');

        $avatar = $request->file('avatar'); // create method to handle this section...
        
        if($request->has('avatar')){
            $merchant->avatar = $request->input('avatar');
        }else if ($request->hasFile('avatar')){
            $extension = $avatar->extension();
            
             $timestamp = time();
    
            $filename = 'MRC_' . $timestamp . '.' . $extension; 
    
            $path = $avatar->storeAs('images', $filename);
    
            if ($request->file('avatar')->isValid()) {
                
                $merchant->avatar = $path;
    
            }
        }
        try{
    
            if ($merchant->save())
            {
    
                $merchant = Merchant::find($merchant->merchant_id);
    
                $merchant->merchant_id = $this->generateCustomerID($merchant->id);   
    
                $merchant->save(); 
    
                // Create Login account
        
                $user->username = $request->input('username');
                $user->password = bcrypt($request->input('password'));
                $user->group_id = 2; // 2 for merchant profile
                $user->details_id = $merchant->merchant_id; 
        
                 $user->save();
        
                return response()->json(['error' => false, 'message' => 'Record added successfully with ID: '. $merchant->merchant_id, 'details_id'=>$merchant->merchant_id],200);
            }
        }catch(Exception $ex){
            return response()->json(['error' => true, 'message' => 'Error inserting, possibly duplicate record'],400);
        }
        return response()->json(['error' => true, 'message' => 'Cannot insert record'],400);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            $merchant = Merchant::findOrFail($id);
            $offers = $merchant->offers()->get()->toArray(); 
            $reviews = $merchant->reviews()->get()->toArray();
            return response()->json(['error' => false, 'merchant' => $merchant, 
                'offers' => $offers, 'reviews' => $reviews],200);
        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],200);
        }

    }
    
    public function convert(){
        
        $merchants = Merchant::orderBy('created_at', 'desc')->get();
        $all = [];
        foreach($merchants as $merchant){
            $address= $merchant['address'];
            $latlng = $this->getCoordinate($address);
            $data = array("name"=>$merchant['name'], "address"=>$merchant['address'], "latlng"=>$latlng);
            array_push($all, $data);
            try{
                $query = "UPDATE merchants SET longlat= '{$latlng}' WHERE id = ".$merchant['id'];
                $upd = DB::statement($query);
                // $m = Merchant::findOrFail($merchant['id']);
                // $m->longlat = $latlng;
                // if($m->save()){
                    
                // }
            }catch (Exception $ex){
                
            }
        }

        return response()->json(['error'=>false, 'merchants'=> $all], 200);
    }
    
    public function getCoordinate($addr){
        //$address = "Ayodeji Oyinlola Street, Ajao Estate, Anthony";
        try{
            $address = str_replace(" ", "+", $addr); // replace all the white space with "+" sign to match with google search pattern
             
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
             
            $response = file_get_contents($url);
             
            $json = json_decode($response,TRUE); //generate array object from the response from the web
             
            $latlng =   $json['results'][0]['geometry']['location']['lat'] .",". $json['results'][0]['geometry']['location']['lng'];
            return $latlng;
        }catch(Exception $e){
            return "6.4531,3.3958";
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $lnglat
     * @return \Illuminate\Http\Response
     */
    public function around($lnglat)
    {
        //Miles = 3959; KM = 6371
        try
        {
            $lnglatArray = explode(",", $lnglat);
            $lng = $lnglatArray[0];
            $lat = $lnglatArray[1];
            $lng_column = "SUBSTRING_INDEX(longlat, ',', -1)";
            $lat_column = "SUBSTRING_INDEX(longlat, ',', 1)";
            $radius = 60;
//             $query = "SELECT *,".
//     "( 6371 * acos( cos( radians({$lat}) ) * cos( radians( SUBSTRING_INDEX(longlat, ',', 2) ) ) * ".
//     "cos( radians( SUBSTRING_INDEX(longlat, ',', 1) ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin( radians( SUBSTRING_INDEX(longlat, ',', 2) ) ) ) ) distance
//  FROM `merchants` HAVING distance <= {$radius} ORDER BY distance ASC";

            $merchant_query = "SELECT *,ROUND(( ".
                "6371 * acos( cos( radians({$lat}) ) ".
                "* cos( radians( $lng_column ) ) ".
                "* cos( radians( $lat_column ) - radians({$lng}) ) + sin( radians({$lat}) ) ".
                "* sin( radians( $lng_column ) ) ) ), 2) AS distance FROM `merchants` ORDER BY distance ASC";

            $merchant = DB::select($merchant_query);// Merchant::findOrFail($id);
            
            $categories = MerchantCategory::orderBy('name', 'asc')->get();
            
            return response()->json(['error' => false, 'merchants' => $merchant, 'categories' => $categories],200);
        }

        catch (Exception $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],200);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {

            $merchant = Merchant::findOrFail($id);

            if ($request->has('category_id'))
                $merchant->category_id = $request->input('category_id');

            if ($request->has('name'))
                $merchant->name = $request->input('name');

            if ($request->has('address'))
                $merchant->address = $request->input('address');

            if ($request->has('contact'))
                $merchant->contact = $request->input('contact');

            if ($request->has('email'))
                $merchant->email = $request->input('email');

            if ($request->has('longlat'))
                $merchant->longlat = $request->input('longlat');

            if ($request->has('verification_pin'))
                $merchant->verification_pin = $request->input('verification_pin');

            if ($request->has('avatar'))
                $merchant->avatar = $request->input('avatar'); 
                
            if ($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $extension = $avatar->extension();
                
                 $timestamp = time();
        
                $filename = 'MRC_' . $timestamp . '.' . $extension; 
        
                $path = $avatar->storeAs('images', $filename);
        
                if ($request->file('avatar')->isValid()) {
                    
                    $merchant->avatar = $path;
        
                }
            }

           if ($merchant->save())
           {
                return response()->json(['error' => false, 'message' => 'Merchant updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating merchant record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $merchant = Merchant::findOrFail($id);


            if ($merchant->delete())
            {

                $user = User::findByDetailsId($id);

                $user->delete();

                return response()->json(['error' => false, 'message' => 'Merchant record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Merchant record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Function to fetch merchant transactions
     *
     * @param  varchar  $merchant_id
     * @return \Illuminate\Http\Response
     */
    public function getTransactions($merchant_id)
    {
        //
        try
        {

            $merchant = Merchant::findOrFail($merchant_id);

            $transactions = $merchant->transactions()->get()->toArray(); 

            return response()->json(['error' => false, 'transactions' => $transactions],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }       
    } 

    /**
     * Function to fetch merchant reviews
     *
     * @param  varchar  $merchant_id
     * @return \Illuminate\Http\Response
     */
    public function getReviews($merchant_id)
    {
        //
        try
        {

            $merchant = Merchant::findOrFail($merchant_id);

            $reviews = $merchant->reviews()->get()->toArray(); 

            return response()->json(['error' => false, 'reviews' => $reviews],200); 
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }         
    } 

    /**
     * Function to fetch merchant offers
     *
     * @param  varchar  $merchant_id
     * @return \Illuminate\Http\Response
     */
    public function getOffers($merchant_id)
    {
        //
        try
        {
            $merchant = Merchant::findOrFail($merchant_id);

            $offers = $merchant->offers()->get()->toArray(); 

            return response()->json(['error' => false, 'offers' => $offers],200); 
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }        
    } 

    /**
     * Function to generate default pin.
     *
     * @return 
     */
    function generateDefaultPin()
    {
     
        $generatedPin = 000000;

        return $generatedPin;

    }

    public function generateCustomerID($id)
    {
     
        $customIdentifier = new CustomIdentifier;

        $generatedID = $customIdentifier->generateCustomID($id, "Merchant");

        return $generatedID;

    }

    public function generatefakeID()
    {
     
        $fake = time();

        return $fake;
    }

}
