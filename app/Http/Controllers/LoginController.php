<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    /**
	 * Handles authentication attempt
	 *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    function authenticate(Request $request)
    {

    	$username = $request->input('username');

    	$password = $request->input('password');

    	if (Auth::attempt(['username' => $username, 'password' => $password]))
    	{
    		//Authentication passed...

    		$user = Auth::user();

    		return response()->json(['error' => false, 'user' => $user], 200);

    	}

    	//Authentication Failed 
        return response()->json(['error' => true, 'message' => 'Unauthenticated: Wrong credentials'], 200);


    }
}
