<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transactions = Transaction::all();

        return response()->json(['error' => false, 'transactions' => $transactions], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $transaction = new Transaction;

        $transaction->customer_id = $request->input('customer_id');

        $transaction->merchant_id = $request->input('merchant_id');

        $transaction->amount = $request->input('amount');

        $transaction->remarks = $request->input('remarks');


           if ($transaction->save())
           {
                return response()->json(['error' => false, 'message' => 'Transaction added successfully with ID: '. $transaction->transaction_id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error adding new transaction'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            $transaction = Transaction::findOrFail($id);

            return response()->json(['error' => false, 'transaction' => $transaction],200);

        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {

            $transaction = Transaction::findOrFail($id);

            if ($request->has('amount'))
                $transaction->amount = $request->input('amount');

            if ($request->has('remarks'))
                $transaction->remarks = $request->input('remarks');

           if ($transaction->save())
           {
                return response()->json(['error' => false, 'message' => 'Transaction updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating transaction record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $transaction = Transaction::findOrFail($id);


            if ($transaction->delete())
            {

            return response()->json(['error' => false, 'message' => 'Transaction record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Transaction record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }
}
