<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all users 
        $users = User::all();

        return response()->json(['error' => false, 'users' => $users], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Save a new user
        $user = new User;

        $user->username = $request->input('username');

        $user->group_id = $request->input('group_id');

        $user->password = bcrypt($request->input('password'));

        // Check if user is not admin ... if yes add extra fields

        if ($user->group_id != 3)
        {
            $user->details_id = $request->input('details_id');
        }

           if ($user->save())
           {
                return response()->json(['error' => false, 'message' => 'User added successfully'],200);
           }

        return response()->json(['error' => true, 'message' => 'Error adding new user'],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
        // show a particular user
        $user = User::findOrFail($id);

        return response()->json(['error' => false, 'user' => $user],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try
        {

            $user = User::findOrFail($id);

        $user->username = $request->input('username');

        $user->password = bcrypt($request->input('password'));

           if ($user->save())
           {
                return response()->json(['error' => false, 'message' => 'User updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating user record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }
    
    public function confirmUsername($username){
        
        if(User::confirmUsername($username) != null){
            return response()->json(['error' => true, 'message' => "Username has been taken, please choose a different one."],400);
        }else{
            return response()->json(['error' => false, 'message' => "Username is available."],200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateByDetailsId(Request $request, $details_id)
    {
        //


        try
        {

            $user = User::findByDetailsId($details_id);

            if ($request->has('username'))
                $user->username = $request->input('username');

            if ($request->has('password'))
                $user->password = bcrypt($request->input('password'));

           if ($user->save())
           {
                return response()->json(['error' => false, 'message' => 'User updated successfully with ID: ' . $details_id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating user record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $user = User::findOrFail($id);


            if ($user->delete())
            {

            return response()->json(['error' => false, 'message' => 'User record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'User record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

}
