<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reviews = Review::orderBy('created_at', 'desc')->get();//::all();

        return response()->json(['error' => false, 'reviews' => $reviews], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $review = new Review;

        $review->customer_id = $request->input('customer_id');

        $review->review = $request->input('review');

        $review->rating = $request->input('rating');
        
        $review->merchant_id = $request->input('merchant_id');


           if ($review->save())
           {
                return response()->json(['error' => false, 'message' => 'Review added successfully with ID: '. $review->review_id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error adding new review'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            $review = Review::findOrFail($id);

            return response()->json(['error' => false, 'review' => $review],200);

        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {

            $review = Review::findOrFail($id);

            if ($request->has('review'))
                $review->review = $request->input('review');

            if ($request->has('rating'))
                $review->rating = $request->input('rating');

           if ($review->save())
           {
                return response()->json(['error' => false, 'message' => 'Review updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating review record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $review = Review::findOrFail($id);


            if ($review->delete())
            {

            return response()->json(['error' => false, 'message' => 'Review record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Review record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }
}
