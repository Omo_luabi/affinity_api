<?php

namespace App\Http\Controllers;

use App\MerchantOffer;
use App\Merchant;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MerchantOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $offers = MerchantOffer::all();
        $offers = DB::table('merchant_offers')->join('merchants', 'merchants.merchant_id', '=', 'merchant_offers.merchant_id')
        ->select('merchant_offers.*', 'merchants.name')->orderBy('merchant_offers.created_at', 'desc')->get();

        return response()->json(['error' => false, 'offers' => $offers],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            //
            $offer = new MerchantOffer;
    
            $offer->merchant_id = $request->input('merchant_id');
            
            $merchant = Merchant::findOrFail($offer->merchant_id);
    
            $offer->details = $request->input('details');
    
            $offer->tagline = $request->input('tagline');
    
            $offer->period = $request->input('period');
            
            if ($request->has('avatar'))
                    $offer->avatar = $request->input('avatar'); 
                    
            if ($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $extension = $avatar->extension();
                
                 $timestamp = time();
        
                $filename = 'OFR_' . $timestamp . '.' . $extension; 
        
                $path = $avatar->storeAs('images', $filename);
        
                if ($request->file('avatar')->isValid()) {
                    
                    $offer->avatar = $path;
        
                }
            }
    
               if ($offer->save())
               {
                    return response()->json(['error' => false, 'message' => 'Offer created successfully with ID: '. $offer->offer_id, 'name' => $merchant->name],200);
               }
        }catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    
        return response()->json(['error' => true, 'message' => 'Error creating new offer'],200);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            $offer = MerchantOffer::findOrFail($id);

            return response()->json(['error' => false, 'offer' => $offer],200);

        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try
        {

        $offer = MerchantOffer::findOrFail($id);


        if ($request->has('details'))
            $offer->details = $request->input('details');

        if ($request->has('tagline'))
            $offer->tagline = $request->input('tagline');

        if ($request->has('period'))
            $offer->period = $request->input('period');
            
        if ($request->has('avatar'))
                $offer->avatar = $request->input('avatar'); 
                
        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $extension = $avatar->extension();
            
             $timestamp = time();
    
            $filename = 'OFR_' . $timestamp . '.' . $extension; 
    
            $path = $avatar->storeAs('images', $filename);
    
            if ($request->file('avatar')->isValid()) {
                
                $offer->avatar = $path;
    
            }
        }

       if ($offer->save())
       {
            return response()->json(['error' => false, 'message' => 'Offer updated successfully with ID: ' . $id],200);
       }

        return response()->json(['error' => true, 'message' => 'Error updating offer record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $offer = MerchantOffer::findOrFail($id);


            if ($offer->delete())
            {

            return response()->json(['error' => false, 'message' => 'Offer record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Offer record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }
}
