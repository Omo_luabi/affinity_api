<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events = Event::all();

        return response()->json(['error' => false, 'events' => $events],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $event = new Event;

        $event->group_id = $request->input('group_id');

        $event->name = $request->input('name');

        $event->description = $request->input('description');

        $event->location = $request->input('location');

        $event->duration = $request->input('duration');
        
        $avatar = $request->file('avatar'); 
        
        if($request->has('avatar')){
            $event->avatar = $request->input('avatar');
        }else if ($request->hasFile('avatar')){
            $extension = $avatar->extension();
    
           // $timestamp = Carbon::now();
            $timestamp = time();
    
            $filename = 'EVN_' . $event->group_id . $timestamp . '.' . $extension; 
    
            $path = $avatar->storeAs('images', $filename);
    
            if ($request->file('avatar')->isValid()) {
                
                $event->avatar = $path;
    
            }
        }

        if ($event->save())
        {
            return response()->json(['error' => false, 'message' => 'Event record added successfully with ID: '. $event->event_id],200);
        }

        return response()->json(['error' => true, 'message' => 'Customer record not successfully added'],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try
        {
            
            $event = Event::findOrFail($id);

            return response()->json(['error' => false, 'event' => $event],200);

        }
        catch (ModelNotFoundException $ex)
        {
            // Record not found... return error message
            return response()->json(['error' => true, 'message' => 'Record not found'],404);

        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {
            $event = Event::findOrFail($id);

            if ($request->has('group_id'))
            $event->group_id = $request->input('group_id');

            if ($request->has('name'))
            $event->name = $request->input('name');

            if ($request->has('description'))
            $event->description = $request->input('description');

            if ($request->has('location'))
            $event->location = $request->input('location');

            if ($request->has('duration'))
            $event->duration = $request->input('duration');
            
            if($request->has('avatar')){
                $event->avatar = $request->input('avatar');
            }else if ($request->hasFile('avatar')){
                $avatar = $request->file('avatar'); 
                
                $extension = $avatar->extension();
        
               // $timestamp = Carbon::now();
                $timestamp = time();
        
                $filename = 'EVN_' . $event->group_id . $timestamp . '.' . $extension; 
        
                $path = $avatar->storeAs('images', $filename);
        
                if ($request->file('avatar')->isValid()) {
                    
                    $event->avatar = $path;
        
                }
            }

           if ($event->save())
           {
                return response()->json(['error' => false, 'message' => 'Event updated successfully with ID: ' . $id],200);
           }

            return response()->json(['error' => true, 'message' => 'Error updating event'],200);
           
        }
        catch (ModelNotFoundException $ex)
        {
            // Not Found... return error message
            return response()->json(['error' => true, 'message' => 'Record not found'],404);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $event = Event::findOrFail($id);


            if ($event->delete())
            {

                return response()->json(['error' => false, 'message' => 'Event record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Event record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {

            return response()->json(['error' => true, 'message' => 'Record not found'],404);

        }
    }

     /**
     * Function to fetch event customers
     *
     * @param  varchar  $event_id
     * @return \Illuminate\Http\Response
     */
    public function getCustomers($event_id)
    {
        //
        try
        {

            $event = Event::findOrFail($event_id);

            $customers = $event->customers()->get()->toArray();

            return response()->json(['error' => false, 'customers' => $customers], 200);

        }
        catch (ModelNotFoundException $ex)
        {

            return response()->json(['error' => true, 'message' => 'Record not found'], 404);

        }

    }
}
