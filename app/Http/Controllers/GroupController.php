<?php

namespace App\Http\Controllers;

use App\Group;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $groups = Group::all();

        return response()->json(['error' => false, 'groups' => $groups], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new Group;

        $group->name = $request->input('name');

        $group->creator_id = $request->input('creator_id'); //confirm if user exists...

        $group->details = $request->input('details');

        $group->group_head_id = $request->input('group_head_id'); // not confirmed... maybe creator_id by default...

        $avatar = $request->file('avatar'); // create method to handle this section...
        
        if($request->has('avatar')){
                $group->avatar = $request->input('avatar');
        }else if ($request->hasFile('avatar')){
            $extension = $avatar->extension();
    
           // $timestamp = Carbon::now();
             $timestamp = time();
    
            $filename = 'GRP_' . $group->creator_id . $timestamp . '.' . $extension; 
    
            $path = $avatar->storeAs('images', $filename);
    
            if ($request->file('avatar')->isValid()) {
                
                $group->avatar = $path;
    
            }
        }
        


       if ($group->save())
       {
            return response()->json(['error' => false, 'message' => 'Group created successfully with ID: '. $group->group_id],200);
       }

        return response()->json(['error' => true, 'message' => 'Error creating new group'],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      try
        {
            $group = Group::findOrFail($id);

            return response()->json(['error' => false, 'group' => $group],200);

        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {
            $group = Group::findOrFail($id);

            if ($request->has('name'))
                $group->name = $request->input('name');


            if ($request->has('details'))
                $group->details = $request->input('details');

            
            if ($request->has('group_head_id'))
                $group->group_head_id = $request->input('group_head_id'); // not confirmed... maybe creator_id by default...

            //if ($request->hasFile('avatar'))
                //$group->avatar = $request->input('avatar'); // create method to handle this section... 
            if($request->has('avatar')){
                $group->avatar = $request->input('avatar');
            }else if ($request->hasFile('avatar')){
                $avatar = $request->file('avatar'); 
                
                $extension = $avatar->extension();
        
               // $timestamp = Carbon::now();
                $timestamp = time();
        
                $filename = 'GRP_' . $group->creator_id . $timestamp . '.' . $extension; 
        
                $path = $avatar->storeAs('images', $filename);
        
                if ($request->file('avatar')->isValid()) {
                    
                    $group->avatar = $path;
        
                }
            }

           if ($group->save())
           {
                return response()->json(['error' => false, 'message' => 'Group updated successfully with ID: ' . $id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error updating group record'],200);

        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $group = Group::findOrFail($id);


            if ($group->delete())
            {

            return response()->json(['error' => false, 'message' => 'Group record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Group record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }


     /**
     * Function to fetch group customers
     *
     * @param  int  $group_id
     * @return \Illuminate\Http\Response
     */
    public function getCustomers($group_id)
    {
        //
        try
        {

            $group = Group::findOrFail($group_id);

            $customers = $group->customers()->get()->toArray();

            return response()->json(['error' => false, 'customers' => $customers], 200);

        }
        catch (ModelNotFoundException $ex)
        {

            return response()->json(['error' => true, 'message' => 'Record not found'], 404);

        }      
    }

    /**
     * Function to fetch group events
     *
     * @param  int  $group_id
     * @return \Illuminate\Http\Response
     */
    public function getEvents($group_id)
    {
        //
        try
        {

            $group = Group::findOrFail($group_id);

            $events = $group->events()->get()->toArray();

            return response()->json(['error' => false, 'events' => $events], 200);

        }
        catch (ModelNotFoundException $ex)
        {

            return response()->json(['error' => true, 'message' => 'Record not found'], 404);

        }       
    }
}
