<?php

namespace App\Http\Controllers;

use App\Interest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class InterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $interests = Interest::all();

        return response()->json(['error' => false, 'interests' => $interests], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $interest = new Interest;

        $interest->name = $request->input('name');

           if ($interest->save())
           {
                return response()->json(['error' => false, 'message' => 'Interest added successfully with ID: '. $interest->interest_id],200);
           }

        return response()->json(['error' => true, 'message' => 'Error adding new interest'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      try
        {
            $interest = Interest::findOrFail($id);

            return response()->json(['error' => false, 'interest' => $interest],200);

        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {
            $interest = Interest::findOrFail($id);

            $interest->name = $request->input('name');

           if ($interest->save())
           {
                return response()->json(['error' => false, 'message' => 'Interest updated successfully with ID: '. $interest->interest_id],200);
           }

            return response()->json(['error' => true, 'message' => 'Error updating interest'],200);
        }

        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {

            $interest = Interest::findOrFail($id);


            if ($interest->delete())
            {

            return response()->json(['error' => false, 'message' => 'Interest record deleted successfully'],200);
            
            }

            return response()->json(['error' => true, 'message' => 'Interest record could not be deleted'],200);
        
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['error' => true, 'message' => 'Record not found'],404);
        }
    }

    /**
     * Function to fetch customers associated with interest
     *
     * @param  int  $interest_id
     * @return \Illuminate\Http\Response
     */
    public function getCustomers($interest_id)
    {
        //
        try
        {

            $interest = Interest::findOrFail($interest_id);

            $customers = $interest->customers()->get()->toArray();

            return response()->json(['error' => false, 'customers' => $customers], 200);

        }
        catch (ModelNotFoundException $ex)
        {

            return response()->json(['error' => true, 'message' => 'Record not found'], 404);

        }       
    }
}
