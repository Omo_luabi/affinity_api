<?php

namespace App\Http\Controllers;

class CustomIdentifier
{
	public $table_id;

	public $type;

	public $generatedID;

	public $prefix;

	function generateCustomID($table_id, $type)
	{
		$expectedLength = 6;

		switch ($type) {
			case 'Merchant':
				# code...
				$prefix = "MR-";
				break;
			
			case 'Customer':
				# code...
				$prefix = "CS-";
				break;
		}

		$neededPadding = $expectedLength - strlen($table_id);

		if ($neededPadding > 2)
		{
			if (strpos(substr($table_id, -1), "1") !== false)
			{
				$table_id = "MA" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "2") !== false)
			 {
				$table_id = "CP" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "3") !== false)
			 {
				$table_id = "NB" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "4") !== false)
			 {
				$table_id = "AM" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "5") !== false)
			 {
				$table_id = "MX" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "6") !== false)
			 {
				$table_id = "PB" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "7") !== false)
			 {
				$table_id = "JB" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "8") !== false)
			 {
				$table_id = "JM" . $table_id;
			}
			elseif (strpos(substr($table_id, -1), "9") !== false)
			 {
				$table_id = "ED" . $table_id;
			}
		}

		$table_id = str_pad($table_id, $expectedLength,"0", STR_PAD_LEFT);

		$generatedID = $prefix . $table_id;

		return $generatedID;
	}

}